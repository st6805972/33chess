import zmq
import os
import json
import time



def server(para: dict):
    global socket,context,socket2,context2
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind(para["host"])
    socket.recv()
    print("connect1")
    # context2 = zmq.Context()
    # socket2 = context2.socket(zmq.REP)
    # socket2.bind(para["host2"])
    # socket.recv()
    # print("connect2")

def client(para: dict):
    global socket,context
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect(para["host"])
    socket.send(B"0")
    print("connect")

# def client2(para: dict):
#     global socket,context
#     context = zmq.Context()
#     socket = context.socket(zmq.REQ)
#     socket.connect(para["host2"])
#     socket.send(B"0")
#     print("connect")

def setmap():
    long, loose = 3, 3
    global chessmap
    chessmap = [[0] * long for i in range(loose)]

def check(key):
    sum=0
    if chessmap[0][0]== key:
        if chessmap[1][1]== key:
            if chessmap[2][2] == key:
                return 3
    if chessmap[0][2]== key:
        if chessmap[1][1]== key:
            if chessmap[2][0] == key:
                return 3
    for x in range (3):
        if chessmap[0][x] == key:
            for y in range(3):
                if chessmap[y][x] ==key:
                    sum+=1
            if sum==3 :
                return 3
            else:
                sum=0
    for y in range(3):
        if chessmap[y][0] == key:
            for x in range(3):
                if chessmap[y][x] == key:
                    sum += 1
            if sum == 3:
                return 3
            else:
                sum = 0
    return 0


if __name__ == '__main__':
    setmap()
    with open("text_for_chess.json", "r") as f:
        para = json.load(f)
    server(para)
    socket.send_string("Gamestart")
    he=socket.recv_string()
    if(he=='X'):
        while(True):
            xx, yy =map(int,input("X Y:").split())
            while xx<0 or xx >2 or yy<0 or yy>2 or chessmap[xx][yy]!=0:
                xx, yy =map(int,input("X Y:").split())
            chessmap[xx][yy]='Y'
            j = {}
            j["0"] = chessmap[0]
            j["1"] = chessmap[1]
            j["2"] = chessmap[2]
            s=json.dumps(j)
            socket.send_json(s)
            if check('Y')==3:
                print("I win")
                time.sleep(1000)

            j = socket.recv_json()
            j = json.loads(j)
            chessmap[0] = j["0"]
            chessmap[1] = j["1"]
            chessmap[2] = j["2"]
            for x in range(3):
                print(chessmap[x])
            if check('X')==3:
                print("lose")
                time.sleep(1000)
    else:
        print("No")
    # while True:
    #     j = {}
    #     j["0"] = chessmap[0]
    #     j["1"] = chessmap[1]
    #     j["2"] = chessmap[2]
    #     s=json.dumps(j)
    #     socket.send_json(s)
    #     j=socket.recv_json()
    #     j=


    # socket2.send("Gamestart")





